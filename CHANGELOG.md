# Gemnasium analyzer changelog

## v1.0.9

- remove container scannings fips on child pipeline. (!9)

## v1.0.8

- release new version. (!8)

## v1.0.7

- fix FIPS container scanning tests. (!6)

## v1.0.6

- add FIPS support. (!5)

## v1.0.5

- another new feature. (!4)

## v1.0.4

- another new feature. (!3)

## v1.0.3

- another new feature. (!2)

## v1.0.2
### gemnasium

- some new feature. (!1)

## v1.0.1
### gemnasium

- Initial release.
